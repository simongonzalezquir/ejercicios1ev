package eva1;

import java.util.Scanner;

public class Multiplicar {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner teclas = new Scanner(System.in);
		int tabla;
		int resultado = 0; 
		int respuesta;
		int fallos;
		int salir=1;
		
		do {
		
			fallos=0;
			
			System.out.print("Introduzca la tabla que desea repasar\n");
			tabla = teclas.nextInt();
			
			
			for (int i =1;i <=10; i++) {						//usamos la variable tabla para elegir el numero a multiplicar
					resultado = i*tabla;
					System.out.printf("Cuanto es %d * %d \n",i,tabla);
					respuesta = teclas.nextInt();
						if(respuesta != resultado) {			//si no acierta le sumamo el fallo
							fallos++;
						}
							
				System.out.printf("resultado %d \n", resultado);	//mostramos resultado
			}
						
		
			if (fallos > 2) {
				System.out.printf("Ha suspendido ha tenido %d fallos \n", fallos); //indicamos los resultados del examen
				
			}
			else
				System.out.print("Enhorabuena ha aprobado \n");
			
			System.out.print("Si desea salir pulse 0 de lo contrario repasaremos otra tabla\n"); //damos la opcion de seguir repasando
			salir=teclas.nextInt();
			
		}while (salir !=0);
	
	
	}
		
}
	
