package eva1;

import java.util.Scanner;

public class AdivinaNumero {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner teclas = new Scanner(System.in);
		
		double numeroMaquina = Math.floor(Math.random()*(100000-1000+1)+1); // generamos un numero entre 100000 y 1000
		
		numeroMaquina = Math.round(numeroMaquina);
		
		int numero = (int)numeroMaquina;							//para que sea un Int y quitarle la coma lo transformamos y guardamos en int
		
		int numeroPerson=0;
		
		
		while (numero != numeroPerson) {
		
			System.out.print("Introduzca el numero que estoy pensando\n ");
		
			numeroPerson=teclas.nextInt();
		
			if (numeroPerson > numero) {
				System.out.printf("El numero %d es mayor que el que estoy pensando\n", numeroPerson);
			}
			else if (numeroPerson < numero) {
				System.out.printf("El numero %d es menor que el que estoy pensando\n", numeroPerson);
			}
			else
				System.out.printf("Enhorabuena el numero %d es igual al que estaba pensando %d \n", numeroPerson, numero);
		}

	}
}
