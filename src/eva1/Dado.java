package eva1;

import java.util.Scanner;

public class Dado {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner teclas = new Scanner(System.in);
		
		System.out.println("cuantas veces se va a tirar el dado?");
		
		int tiradas = teclas.nextInt();		//numero de veces que se tira el dado
		
		int numeros[]  = new int[6];		//declaramos array de 6 posiciones
		
		for (int i=0;i<tiradas;i++) {		//lanzamos la veces que se diga
		
			double tirada = Math.floor(Math.random()*(6-1+1)+1);	//sera un numero entre 1 y 6
			int numero = (int) tirada;								//quitamos la coma
			numeros[numero-1]=numeros[numero-1]+1;			//para que este en el indice 0-5 restamos uno a la posicion
		}													//y sumamos 1 al valor inicial que sera 0
															//para contar el numero de veces que salio 
					
		for (int j=0;j<numeros.length;j++) {			//recorremos el array
			System.out.printf("la cara %d ha salido %d veces \n",j+1, numeros[j]); //mostramos el valor
		}

}
}


