package eva1;

import java.util.Scanner;

public class TrianguloFloyd {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	Scanner teclas = new Scanner(System.in);
	
	System.out.print("Inserte el numero de filas\n");
	
	int filas = teclas.nextInt();
	
	int total = 0;							//lo usaremos para imprimir los numeros de la piramide 
	
	for (int i = 0; i<filas;i++) {			//este nos imprimira las filas 
		for (int j = i+1;j>0;j--) {			//este nos imprimira los numeros
			total = total+1;				//le agregamos uno al total para que en la siguiente iteracion no se repita el numero
			System.out.print(total+" ");	
		} 
		System.out.println();				//metemos el salto de linea en el bucle de i que es el que imprime las filas 
	}
	
	
	
	}

}
