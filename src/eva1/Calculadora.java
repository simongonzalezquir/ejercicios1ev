package eva1;

import java.util.Scanner;

public class Calculadora {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
			Scanner teclas = new Scanner(System.in);
			int salir =1;
			do {
			System.out.print("Introduzca el primer numero \n");
			
			int numero1 = teclas.nextInt();
			
			System.out.print("Introduzca el operador \n");
			
			String op = teclas.next();
			
			System.out.print("Introduzca segundo numero \n");
			
			int numero2 = teclas.nextInt();
			
			int resultado = 0;
						
			
			switch (op) {
			
				case "+":
					resultado = numero1 + numero2;
					System.out.printf("%d %s %d = %d \n",numero1, op, numero2, resultado);
					break;
				case "-":
					resultado = numero1 - numero2;
					System.out.printf("%d %s %d = %d \n",numero1, op, numero2, resultado);
					break;
				case "*":
					resultado = numero1 * numero2;
					System.out.printf("%d %s %d = %d \n",numero1, op, numero2, resultado);
					break;
				case "/":
					if (numero2 != 0) {
						resultado = numero1 / numero2;
						System.out.printf("%d %s %d = %d \n",numero1, op, numero2, resultado);
					}
					else
						System.out.print("No se puede dividir entre 0");
					break;
					
				case "%":
					if(numero2 !=0) {
						resultado = numero1 % numero2;
						System.out.printf("%d %s %d = %d \n",numero1, op, numero2, resultado);
					}
						else
						System.out.print("No se puede dividir entre 0");
						break;
				default:
					System.out.print("El operador debe ser '+', '-', '*', '/' o '%'");
			
			}
			
			System.out.print("0 para salir, otro numero para continuar \n");
			salir = teclas.nextInt();							//controlar que sea un numero?�
			
			}while(salir != 0);
			
	}

}
