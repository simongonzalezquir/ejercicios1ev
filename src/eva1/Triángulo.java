package eva1;

import java.util.Scanner;

public class Tri�ngulo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner teclas = new Scanner (System.in);
		
		System.out.print("Vamos a ver si podemos hacer un tri�ngulo con 3 numeros que me des \nIntroduzca el primer numero\n");
		int a = teclas.nextInt();
		System.out.print("Introduzca el segundo numero\n");
		int b = teclas.nextInt();
		System.out.print("Introduzca el tercer numero\n");
		int c = teclas.nextInt();
		
		if (a + b > c && b + c > a && c + a > b ){			//asi vemos si puede ser triangulo la suma de 2 mayor que el restante
			
			if (a == b && b == c) {							//si todos los lados son iguales es equil�tero
				System.out.println("El tri�ngulo es equil�tero");
			}
			else if (a == b || b == c) {					//si 2 lados son iguales es is�sceles
				
				System.out.println("El tri�ngulo es is�sceles");
			}
			
			else											//si no tiene lados iguales es escaleno
				System.out.println("El tri�ngulo es escaleno");
			
			
		}
		
		else 
			System.out.print("No se puede hacer el tri�ngulo");
	
	}

}
